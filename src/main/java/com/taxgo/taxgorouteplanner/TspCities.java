package com.taxgo.taxgorouteplanner;

import com.google.ortools.Loader;
import com.google.ortools.constraintsolver.Assignment;
import com.google.ortools.constraintsolver.FirstSolutionStrategy;
import com.google.ortools.constraintsolver.LocalSearchMetaheuristic;

import com.google.ortools.constraintsolver.RoutingIndexManager;
import com.google.ortools.constraintsolver.RoutingModel;
import com.google.ortools.constraintsolver.RoutingSearchParameters;
import com.google.ortools.constraintsolver.main;
import com.google.protobuf.Duration;

import java.util.ArrayList;
import java.util.logging.Logger;

//import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/** Minimal TSP using distance matrix. */
@RestController
public class TspCities {
	private static final Logger logger = Logger.getLogger(TspCities.class.getName());

	/// @brief Print the solution.
	static TempResponse printSolution(RoutingModel routing, RoutingIndexManager manager, Assignment solution) {
		ArrayList<Integer> routeArray = new ArrayList<Integer>();// Creating arraylist
		// Solution cost.
		logger.info("Objective: " + solution.objectiveValue() + "miles");
		// Inspect solution.
		logger.info("Route:");
		long routeDistance = 0;
		String route = "";
		long index = routing.start(0);
		while (!routing.isEnd(index)) {
			int indexToNode = manager.indexToNode(index);
			routeArray.add(indexToNode);
			route += indexToNode + " -> ";
			long previousIndex = index;
			index = solution.value(routing.nextVar(index));
			routeDistance += routing.getArcCostForVehicle(previousIndex, index, 0);
		}
		int indexToEnd = manager.indexToNode(routing.end(0));
		routeArray.add(indexToEnd);
		route += indexToEnd;
		logger.info(route);
		logger.info("Route distance: " + routeDistance + "miles");
		TempResponse tempResponse = new TempResponse();
		tempResponse.setRouteArray(routeArray);
		tempResponse.setTotalDistance(routeDistance);
		return tempResponse;
	}

	/**
	 * Calculate distance between two points in latitude and longitude taking into
	 * account height difference. If you are not interested in height difference
	 * pass 0.0. Uses Haversine method as its base.
	 * 
	 * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters el2
	 * End altitude in meters
	 * 
	 * @returns Distance in Meters
	 */
	public static long distance(Location startLoc, Location endLoc) {
		double lat1 = startLoc.getLatitude();
		double lat2 = endLoc.getLatitude();
		double lon1 = startLoc.getLongitude();
		double lon2 = startLoc.getLongitude();
		final int R = 6371; // Radius of the earth

		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lon2 - lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c * 1000; // convert to meters

		double height = 0;

		distance = Math.pow(distance, 2) + Math.pow(height, 2);

		return (long) (Math.sqrt(distance) * 1000);
	}

	@PostMapping(path = "/tsp", consumes = "application/json", produces = "application/json")
	public Response solveTsp(@RequestBody Request request) {
		Response response = new Response();
		try {
			ObjectMapper mapper = new ObjectMapper();

			TempResponse t;
			ArrayList<Integer> routeArray = new ArrayList<Integer>();// Creating arraylist
			ArrayList<String> routeAddressArray = new ArrayList<String>();
			Loader.loadNativeLibraries();

			// Create Routing Index Manager
//		RoutingIndexManager manager = new RoutingIndexManager(data.distanceMatrix.length, data.vehicleNumber,
//				data.depot);

			int startNode = Integer.parseInt(request.getStartLocation());
			int endNode = Integer.parseInt(request.getEndLocation());
			ArrayList<Location> locations = request.getLocations();
			// create distance matrix
			long[][] distanceMatrix = new long[locations.size()][locations.size()];
			for (int ii = 0; ii < locations.size(); ii++) {
				for (int jj = 0; jj < locations.size(); jj++) {
					long distance = distance(locations.get(ii), locations.get(jj));
					distanceMatrix[ii][jj] = distance;
				}
			}

			int[] starts = { startNode };// can be more start and end locaitons based on the number of vehicle and each
											// vehicles start and end
			int[] ends = { endNode }; // can be more start and end locaitons based on the number of vehicle and each
										// vehicles start and end
			int vehicleNumber = 1;// it can be more than one so that it will become a vehicle routing problem
//		long[][] distanceMatrix = { { 0, 2451, 713, 1018, 1631, 1374, 2408, 213, 2571, 875, 1420, 2145, 1972 },
//				{ 2451, 0, 1745, 1524, 831, 1240, 959, 2596, 403, 1589, 1374, 357, 579 },
//				{ 713, 1745, 0, 355, 920, 803, 1737, 851, 1858, 262, 940, 1453, 1260 },
//				{ 1018, 1524, 355, 0, 700, 862, 1395, 1123, 1584, 466, 1056, 1280, 987 },
//				{ 1631, 831, 920, 700, 0, 663, 1021, 1769, 949, 796, 879, 586, 371 },
//				{ 1374, 1240, 803, 862, 663, 0, 1681, 1551, 1765, 547, 225, 887, 999 },
//				{ 2408, 959, 1737, 1395, 1021, 1681, 0, 2493, 678, 1724, 1891, 1114, 701 },
//				{ 213, 2596, 851, 1123, 1769, 1551, 2493, 0, 2699, 1038, 1605, 2300, 2099 },
//				{ 2571, 403, 1858, 1584, 949, 1765, 678, 2699, 0, 1744, 1645, 653, 600 },
//				{ 875, 1589, 262, 466, 796, 547, 1724, 1038, 1744, 0, 679, 1272, 1162 },
//				{ 1420, 1374, 940, 1056, 879, 225, 1891, 1605, 1645, 679, 0, 1017, 1200 },
//				{ 2145, 357, 1453, 1280, 586, 887, 1114, 2300, 653, 1272, 1017, 0, 504 },
//				{ 1972, 579, 1260, 987, 371, 999, 701, 2099, 600, 1162, 1200, 504, 0 }, };

			RoutingIndexManager manager = new RoutingIndexManager(distanceMatrix.length, vehicleNumber, starts, ends);

			// Create Routing Model.
			RoutingModel routing = new RoutingModel(manager);

			// Create and register a transit callback.
			final int transitCallbackIndex = routing.registerTransitCallback((long fromIndex, long toIndex) -> {
				// Convert from routing variable Index to user NodeIndex.
				int fromNode = manager.indexToNode(fromIndex);
				int toNode = manager.indexToNode(toIndex);
				return distanceMatrix[fromNode][toNode];
			});

			// Define cost of each arc.
			routing.setArcCostEvaluatorOfAllVehicles(transitCallbackIndex);

			// Setting first solution heuristic.
//			RoutingSearchParameters searchParameters = main.defaultRoutingSearchParameters().toBuilder()
//					.setFirstSolutionStrategy(FirstSolutionStrategy.Value.AUTOMATIC)
//					.setLocalSearchMetaheuristic(LocalSearchMetaheuristic.Value.AUTOMATIC)
//					.build();
//			RoutingSearchParameters searchParameters =  main.defaultRoutingSearchParameters()
//			         .toBuilder()
//			         .setFirstSolutionStrategy(FirstSolutionStrategy.Value.SAVINGS)
//			         .setLocalSearchMetaheuristic(searchValue)
//			         .setTimeLimit(Duration.newBuilder().setSeconds(30).build())
//			         .build();

			RoutingSearchParameters searchParameters;

			
			com.google.ortools.constraintsolver.LocalSearchMetaheuristic.Value searchValue;
			
			if(request.getSrchAlg().equals("AUTOMATIC")) {
				searchValue= LocalSearchMetaheuristic.Value.AUTOMATIC;
			}
			else if(request.getSrchAlg().equals("GREEDY_DESCENT")) {
				searchValue= LocalSearchMetaheuristic.Value.GREEDY_DESCENT;
			}
			else if(request.getSrchAlg().equals("GUIDED_LOCAL_SEARCH")) {
				searchValue= LocalSearchMetaheuristic.Value.GUIDED_LOCAL_SEARCH;
			}
			else if(request.getSrchAlg().equals("SIMULATED_ANNEALING")) {
				searchValue= LocalSearchMetaheuristic.Value.SIMULATED_ANNEALING;
			}
			else if(request.getSrchAlg().equals("TABU_SEARCH")) {
				searchValue= LocalSearchMetaheuristic.Value.TABU_SEARCH;
			}
			else if(request.getSrchAlg().equals("OBJECTIVE_TABU_SEARCH")) {
				searchValue= LocalSearchMetaheuristic.Value.GENERIC_TABU_SEARCH;
			}
			else{
				searchValue= LocalSearchMetaheuristic.Value.AUTOMATIC;
			}
			
			
			if (request.getAlg().equals("AUTOMATIC")) {
				searchParameters = main.defaultRoutingSearchParameters().toBuilder()
						.setFirstSolutionStrategy(FirstSolutionStrategy.Value.AUTOMATIC)
						.setLocalSearchMetaheuristic(searchValue)
						.setTimeLimit(Duration.newBuilder().setSeconds(30).build()).build();
			} else if (request.getAlg().equals("PATH_CHEAPEST_ARC")) {
				searchParameters = main.defaultRoutingSearchParameters().toBuilder()
						.setFirstSolutionStrategy(FirstSolutionStrategy.Value.PATH_CHEAPEST_ARC)
						.setLocalSearchMetaheuristic(searchValue)
						.setTimeLimit(Duration.newBuilder().setSeconds(30).build()).build();
			} else if (request.getAlg().equals("PATH_MOST_CONSTRAINED_ARC")) {
				searchParameters = main.defaultRoutingSearchParameters().toBuilder()
						.setFirstSolutionStrategy(FirstSolutionStrategy.Value.PATH_MOST_CONSTRAINED_ARC)
						.setLocalSearchMetaheuristic(searchValue)
						.setTimeLimit(Duration.newBuilder().setSeconds(30).build()).build();
			} else if (request.getAlg().equals("EVALUATOR_STRATEGY")) {
				searchParameters = main.defaultRoutingSearchParameters().toBuilder()
						.setFirstSolutionStrategy(FirstSolutionStrategy.Value.EVALUATOR_STRATEGY)
						.setLocalSearchMetaheuristic(searchValue)
						.setTimeLimit(Duration.newBuilder().setSeconds(30).build()).build();
			} else if (request.getAlg().equals("SAVINGS")) {
				searchParameters = main.defaultRoutingSearchParameters().toBuilder()
						.setFirstSolutionStrategy(FirstSolutionStrategy.Value.SAVINGS)
						.setLocalSearchMetaheuristic(searchValue)
						.setTimeLimit(Duration.newBuilder().setSeconds(30).build()).build();
			} else if (request.getAlg().equals("SWEEP")) {
				searchParameters = main.defaultRoutingSearchParameters().toBuilder()
						.setFirstSolutionStrategy(FirstSolutionStrategy.Value.SWEEP)
						.setLocalSearchMetaheuristic(searchValue)
						.setTimeLimit(Duration.newBuilder().setSeconds(30).build()).build();
			} else if (request.getAlg().equals("CHRISTOFIDES")) {
				searchParameters = main.defaultRoutingSearchParameters().toBuilder()
						.setFirstSolutionStrategy(FirstSolutionStrategy.Value.CHRISTOFIDES)
						.setLocalSearchMetaheuristic(searchValue)
						.setTimeLimit(Duration.newBuilder().setSeconds(30).build()).build();
			} else if (request.getAlg().equals("ALL_UNPERFORMED")) {
				searchParameters = main.defaultRoutingSearchParameters().toBuilder()
						.setFirstSolutionStrategy(FirstSolutionStrategy.Value.ALL_UNPERFORMED)
						.setLocalSearchMetaheuristic(searchValue)
						.setTimeLimit(Duration.newBuilder().setSeconds(30).build()).build();
			} else if (request.getAlg().equals("BEST_INSERTION")) {
				searchParameters = main.defaultRoutingSearchParameters().toBuilder()
						.setFirstSolutionStrategy(FirstSolutionStrategy.Value.BEST_INSERTION)
						.setLocalSearchMetaheuristic(searchValue)
						.setTimeLimit(Duration.newBuilder().setSeconds(30).build()).build();
			} else if (request.getAlg().equals("PARALLEL_CHEAPEST_INSERTION")) {
				searchParameters = main.defaultRoutingSearchParameters().toBuilder()
						.setFirstSolutionStrategy(FirstSolutionStrategy.Value.PARALLEL_CHEAPEST_INSERTION)
						.setLocalSearchMetaheuristic(searchValue)
						.setTimeLimit(Duration.newBuilder().setSeconds(30).build()).build();
			} else if (request.getAlg().equals("LOCAL_CHEAPEST_INSERTION")) {
				searchParameters = main.defaultRoutingSearchParameters().toBuilder()
						.setFirstSolutionStrategy(FirstSolutionStrategy.Value.LOCAL_CHEAPEST_INSERTION)
						.setLocalSearchMetaheuristic(searchValue)
						.setTimeLimit(Duration.newBuilder().setSeconds(30).build()).build();
			} else if (request.getAlg().equals("GLOBAL_CHEAPEST_ARC")) {
				searchParameters = main.defaultRoutingSearchParameters().toBuilder()
						.setFirstSolutionStrategy(FirstSolutionStrategy.Value.GLOBAL_CHEAPEST_ARC)
						.setLocalSearchMetaheuristic(searchValue)
						.setTimeLimit(Duration.newBuilder().setSeconds(30).build()).build();
			}

			else if (request.getAlg().equals("LOCAL_CHEAPEST_ARC")) {
				searchParameters = main.defaultRoutingSearchParameters().toBuilder()
						.setFirstSolutionStrategy(FirstSolutionStrategy.Value.LOCAL_CHEAPEST_ARC)
						.setLocalSearchMetaheuristic(searchValue)
						.setTimeLimit(Duration.newBuilder().setSeconds(30).build()).build();
			}

			else if (request.getAlg().equals("FIRST_UNBOUND_MIN_VALUE")) {
				searchParameters = main.defaultRoutingSearchParameters().toBuilder()
						.setFirstSolutionStrategy(FirstSolutionStrategy.Value.FIRST_UNBOUND_MIN_VALUE)
						.setLocalSearchMetaheuristic(searchValue)
						.setTimeLimit(Duration.newBuilder().setSeconds(30).build()).build();
			}

			else {
				searchParameters = main.defaultRoutingSearchParameters().toBuilder()
						.setFirstSolutionStrategy(FirstSolutionStrategy.Value.AUTOMATIC)
						.setLocalSearchMetaheuristic(searchValue)
						.setTimeLimit(Duration.newBuilder().setSeconds(30).build()).build();
			}

			// Solve the problem.
			Assignment solution = routing.solveWithParameters(searchParameters);

			// Print solution on console.
			t = printSolution(routing, manager, solution);
			for (int i = 0; i < routeArray.size(); i++) {
				int index = routeArray.get(i);
				routeAddressArray.add(request.getLocations().get(index).getAddress());
			}
			// setting response object
//			response.setRequest(request);
//			response.setOrder(t.getRouteArray());
//			response.setAddressOrder(routeAddressArray);
			response.setTotalDidstance(request.getAlg() + "===" + (t.getTotalDistance() ));

			// Java objects to JSON string - pretty-print
			String requestJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request);

			String respJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(routeAddressArray);
//			System.out.println("*********************** START *************************************");
//			System.out.println("length of locations :- " + locations.size());
//			System.out.println("startNode :- " + startNode);
//			System.out.println("endNode :- " + endNode);
//			System.out.println(requestJson);
//			System.out.println(respJson);
//			System.out.println("*********************** END *************************************");
		} catch (Exception e) {
			System.out.println(e);
		}
		return response;
	}

}
