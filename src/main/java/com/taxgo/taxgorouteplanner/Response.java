package com.taxgo.taxgorouteplanner;

import java.util.ArrayList;


public class Response {
     private ArrayList<Integer> order;
     private ArrayList<String> addressOrder;
     private Request request;
     private String totalDidstance;
	public ArrayList<Integer> getOrder() {
		return order;
	}
	public void setOrder(ArrayList<Integer> order) {
		this.order = order;
	}
	public Request getRequest() {
		return request;
	}
	public void setRequest(Request request) {
		this.request = request;
	}
	public String getTotalDidstance() {
		return totalDidstance;
	}
	public void setTotalDidstance(String totalDidstance) {
		this.totalDidstance = totalDidstance;
	}
	public ArrayList<String> getAddressOrder() {
		return addressOrder;
	}
	public void setAddressOrder(ArrayList<String> addressOrder) {
		this.addressOrder = addressOrder;
	}
     
	
    
}
