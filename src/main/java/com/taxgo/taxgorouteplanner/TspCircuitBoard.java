package com.taxgo.taxgorouteplanner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.ortools.Loader;
import com.google.ortools.constraintsolver.Assignment;
import com.google.ortools.constraintsolver.LocalSearchMetaheuristic;

import com.google.ortools.constraintsolver.FirstSolutionStrategy;
import com.google.ortools.constraintsolver.RoutingIndexManager;
import com.google.ortools.constraintsolver.RoutingModel;
import com.google.ortools.constraintsolver.RoutingSearchParameters;
import com.google.ortools.constraintsolver.main;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.protobuf.Duration;

import io.sentry.Sentry;

/** Minimal TSP. */
@RestController
public class TspCircuitBoard {
	private static final Logger logger = LoggerFactory.getLogger(TspCircuitBoard.class);

	static class DataModel {

		public final int vehicleNumber = 1;
		public final int depot = 0;
	}

	/// @brief Compute Euclidean distance matrix from locations array.
	/// @details It uses an array of locations and computes
	/// the Euclidean distance between any two locations.

	/// @brief Print the solution.
	static TempResponse printSolution(RoutingModel routing, RoutingIndexManager manager, Assignment solution) {
		ArrayList<Integer> routeArray = new ArrayList<Integer>();// Creating arraylist
		// Solution cost.
		logger.info("Objective: " + solution.objectiveValue());
		// Inspect solution.
		logger.info("Route:");
		long routeDistance = 0;
		String route = "";
		long index = routing.start(0);
		while (!routing.isEnd(index)) {

			int indexToNode = manager.indexToNode(index);
			routeArray.add(indexToNode);
			route += indexToNode + " -> ";
			long previousIndex = index;
			index = solution.value(routing.nextVar(index));
			routeDistance += routing.getArcCostForVehicle(previousIndex, index, 0);
		}
		int indexToEnd = manager.indexToNode(routing.end(0));
		routeArray.add(indexToEnd);
		route += indexToEnd;
		logger.info(route);
		logger.info("Route distance: " + routeDistance);
		TempResponse tempResponse = new TempResponse();
		tempResponse.setRouteArray(routeArray);
		tempResponse.setTotalDistance(routeDistance);
		return tempResponse;
	}

	// haversine implemented
	public static long distance(double[] startLoc, double[] endLoc) {
		double lat1 = startLoc[0];
		double lat2 = endLoc[0];
		double lon1 = startLoc[1];
		double lon2 = endLoc[1];
		final int R = 6371; // Radius of the earth

		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lon2 - lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c * 1000; // convert to meters

		double height = 0;

		distance = Math.pow(distance, 2) + Math.pow(height, 2);

		return (long) (Math.sqrt(distance) * 1000);
	}

	@PostMapping(path = "/tspCircuitBoard", consumes = "application/json", produces = "application/json")
	public Response solveTsp(@RequestBody Request request) {
		Response response = new Response();
		try {
			logger.info("start solving tsp ");
			ObjectMapper mapper = new ObjectMapper();

			TempResponse t;
			ArrayList<Integer> routeArray = new ArrayList<Integer>();// Creating arraylist
			ArrayList<String> routeAddressArray = new ArrayList<String>();
			Loader.loadNativeLibraries();

			int vehicleNumber = 1;
			int depot = 0;

			int startNode = Integer.parseInt(request.getStartLocation());
			int endNode = Integer.parseInt(request.getEndLocation());
			ArrayList<Location> locationsRaw = request.getLocations();
			double[][] locations = new double[locationsRaw.size()][locationsRaw.size()];

			for (int i = 0; i < locationsRaw.size(); i++) {
				double[] tempLatLong = new double[2];
				tempLatLong[0] = locationsRaw.get(i).getLatitude();
				tempLatLong[1] = locationsRaw.get(i).getLongitude();
				locations[i] = tempLatLong;
			}

			// Create Routing Index Manager
			RoutingIndexManager manager = new RoutingIndexManager(locations.length, vehicleNumber, depot);

			// Create Routing Model.
			RoutingModel routing = new RoutingModel(manager);
//			final long[][] distanceMatrix = new long[data.locations.length][data.locations.length];
			long[][] distanceMatrix = new long[locations.length][locations.length];
			for (int i = 0; i < locations.length; i++) {
				for (int j = 0; j < locations.length; j++) {
					if (i == j) {
						distanceMatrix[i][j] = 0;
					} else if ((i < j)) {
						distanceMatrix[i][j] = distance(locations[i], locations[j]);
					} else {
						distanceMatrix[i][j] = distanceMatrix[j][i];
					}
				}
			}

			// Create and register a transit callback.

			final int transitCallbackIndex = routing.registerTransitCallback((long fromIndex, long toIndex) -> {
				// Convert from routing variable Index to user NodeIndex.
				int fromNode = manager.indexToNode(fromIndex);
				int toNode = manager.indexToNode(toIndex);
				return distanceMatrix[fromNode][toNode];
			});

			// Define cost of each arc.
			routing.setArcCostEvaluatorOfAllVehicles(transitCallbackIndex);

			// Setting first solution heuristic.
//	    RoutingSearchParameters searchParameters =
//	        main.defaultRoutingSearchParameters()
//	            .toBuilder()
//	            .setFirstSolutionStrategy(FirstSolutionStrategy.Value.PATH_CHEAPEST_ARC)
//	            .build();
			RoutingSearchParameters searchParameters = main.defaultRoutingSearchParameters().toBuilder()
					.setFirstSolutionStrategy(FirstSolutionStrategy.Value.PATH_CHEAPEST_ARC)
					.setLocalSearchMetaheuristic(LocalSearchMetaheuristic.Value.GUIDED_LOCAL_SEARCH)
					.setTimeLimit(Duration.newBuilder().setSeconds(5).build()).setLogSearch(false).build();

			// Solve the problem.
			Assignment solution = routing.solveWithParameters(searchParameters);

			// Print solution on console.
			t = printSolution(routing, manager, solution);
			routeArray = t.getRouteArray();
			for (int i = 0; i < routeArray.size(); i++) {
				int index = routeArray.get(i);
				routeAddressArray.add(request.getLocations().get(index).getAddress());
			}
//			response.setRequest(request);
			response.setOrder(t.getRouteArray());
			response.setAddressOrder(routeAddressArray);
			response.setTotalDidstance("" + t.getTotalDistance());
			logger.info("done solving tsp ");
		} catch (Exception e) {
			logger.info("Error occuered on solving tsp ");
			Sentry.captureException(e);
		}
		return response;

	}
}