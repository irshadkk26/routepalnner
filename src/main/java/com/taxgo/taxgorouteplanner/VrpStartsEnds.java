package com.taxgo.taxgorouteplanner;

import com.google.ortools.Loader;
import com.google.ortools.constraintsolver.Assignment;
import com.google.ortools.constraintsolver.FirstSolutionStrategy;
import com.google.ortools.constraintsolver.LocalSearchMetaheuristic;
import com.google.ortools.constraintsolver.RoutingDimension;
import com.google.ortools.constraintsolver.RoutingIndexManager;
import com.google.ortools.constraintsolver.RoutingModel;
import com.google.protobuf.Duration;
import com.google.ortools.constraintsolver.RoutingSearchParameters;
import com.google.ortools.constraintsolver.main;

import java.util.ArrayList;
import java.util.logging.Logger;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/** Minimal VRP. */
@RestController
public class VrpStartsEnds {
	private static final Logger logger = Logger.getLogger(VrpStartsEnds.class.getName());
 
	/**
	 * Calculate distance between two points in latitude and longitude taking into
	 * account height difference. If you are not interested in height difference
	 * pass 0.0. Uses Haversine method as its base.
	 * 
	 * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters el2
	 * End altitude in meters
	 * 
	 * @returns Distance in Meters
	 */
	public static long distance(Location startLoc, Location endLoc) {
		double lat1 = startLoc.getLatitude();
		double lat2 = endLoc.getLatitude();
		double lon1 = startLoc.getLongitude();
		double lon2 = startLoc.getLongitude();
		final int R = 6371; // Radius of the earth

		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lon2 - lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c * 1000; // convert to meters

		double height = 0;

		distance = Math.pow(distance, 2) + Math.pow(height, 2);

		return (long) Math.sqrt(distance);
	}

	/// @brief Print the solution.
	static void printSolution(int vehicleNumber, RoutingModel routing, RoutingIndexManager manager,
			Assignment solution) {
		// Solution cost.
		logger.info("Objective : " + solution.objectiveValue());
		// Inspect solution.
		long maxRouteDistance = 0;
		int i = 0;
//		for (int i = 0; i < vehicleNumber; ++i) {
			long index = routing.start(i);
			logger.info("Route for Vehicle " + i + ":");
			long routeDistance = 0;
			String route = "";
			while (!routing.isEnd(index)) {
				route += manager.indexToNode(index) + " -> ";
				long previousIndex = index;
				index = solution.value(routing.nextVar(index));
				routeDistance += routing.getArcCostForVehicle(previousIndex, index, i);
			}
			logger.info(route + manager.indexToNode(index));
			logger.info("Distance of the route: " + routeDistance + "m");
			maxRouteDistance = Math.max(routeDistance, maxRouteDistance);
//		}
		logger.info("Maximum of the route distances: " + maxRouteDistance + "m");
	}

	@PostMapping(path = "/tspitoj", consumes = "application/json", produces = "application/json")
	public Response solveTspItoJ(@RequestBody RequestVrp request) {

		int startNode = Integer.parseInt(request.getStartLocation());
		int endNode = Integer.parseInt(request.getEndLocation());
		int vehicleNumber = Integer.parseInt(request.getVehicleNumber());
		ArrayList<Location> locations = request.getLocations();

		// create distance matrix
		long[][] distanceMatrix = new long[locations.size()][locations.size()];
		for (int ii = 0; ii < locations.size(); ii++) {
			for (int jj = 0; jj < locations.size(); jj++) {
				long distance = distance(locations.get(ii), locations.get(jj));
				distanceMatrix[ii][jj] = distance;
			}
		}
		
		System.out.println("========");
		System.out.println(distanceMatrix[2][2]);
		System.out.println("========");

		final int[] starts = { startNode };
		final int[] ends = { endNode };
		Response response = new Response();
		Loader.loadNativeLibraries(); 

		// Create Routing Index Manager
		RoutingIndexManager manager = new RoutingIndexManager(distanceMatrix.length,vehicleNumber, starts,
				ends);

		// Create Routing Model.
		RoutingModel routing = new RoutingModel(manager);

		// Create and register a transit callback.
		final int transitCallbackIndex = routing.registerTransitCallback((long fromIndex, long toIndex) -> {
			// Convert from routing variable Index to user NodeIndex.
			int fromNode = manager.indexToNode(fromIndex);
			int toNode = manager.indexToNode(toIndex);
			return distanceMatrix[fromNode][toNode];
		});

		// Define cost of each arc.
		routing.setArcCostEvaluatorOfAllVehicles(transitCallbackIndex);

		// Add Distance constraint.
		routing.addDimension(transitCallbackIndex, 0, 2000, true, // start cumul to zero
				"Distance");
		RoutingDimension distanceDimension = routing.getMutableDimension("Distance");
		distanceDimension.setGlobalSpanCostCoefficient(100);

		// Setting first solution heuristic.
//		RoutingSearchParameters searchParameters = main.defaultRoutingSearchParameters().toBuilder()
//				.setFirstSolutionStrategy(FirstSolutionStrategy.Value.SAVINGS)
//				.setLocalSearchMetaheuristic(LocalSearchMetaheuristic.Value.AUTOMATIC)
//				.build();
		
//		RoutingSearchParameters searchParameters =  main.defaultRoutingSearchParameters()
//         .toBuilder()
//         .setFirstSolutionStrategy(FirstSolutionStrategy.Value.PATH_MOST_CONSTRAINED_ARC)
//         .setLocalSearchMetaheuristic(LocalSearchMetaheuristic.Value.GUIDED_LOCAL_SEARCH)
//         .setTimeLimit(Duration.newBuilder().setSeconds(30).build())
//         .setLogSearch(true)
//         .build();
		
		RoutingSearchParameters searchParameters =  main.defaultRoutingSearchParameters()
		         .toBuilder()
		         .setFirstSolutionStrategy(FirstSolutionStrategy.Value.PATH_MOST_CONSTRAINED_ARC)
		         .setLocalSearchMetaheuristic(LocalSearchMetaheuristic.Value.GUIDED_LOCAL_SEARCH)
		         .setTimeLimit(Duration.newBuilder().setSeconds(30).build())
		         .build();

		// Solve the problem.
		Assignment solution = routing.solveWithParameters(searchParameters);

		// Print solution on console.
		printSolution(vehicleNumber, routing, manager, solution);
		return response;
	}
}