package com.taxgo.taxgorouteplanner;

import java.util.ArrayList;


public class Request {
    private String startLocation;
    private String endLocation;
    private ArrayList<Location> locations;
    private String alg;
    private String srchAlg;
	public String getStartLocation() {
		return startLocation;
	}
	public void setStartLocation(String startLocation) {
		this.startLocation = startLocation;
	}
	public String getEndLocation() {
		return endLocation;
	}
	public void setEndLocation(String endLocation) {
		this.endLocation = endLocation;
	}
	public ArrayList<Location> getLocations() {
		return locations;
	}
	public void setLocations(ArrayList<Location> locations) {
		this.locations = locations;
	}
	public String getAlg() {
		return alg;
	}
	public void setAlg(String alg) {
		this.alg = alg;
	}
	public String getSrchAlg() {
		return srchAlg;
	}
	public void setSrchAlg(String srchAlg) {
		this.srchAlg = srchAlg;
	}
	
	
    
}
