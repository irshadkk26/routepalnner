package com.taxgo.taxgorouteplanner;

import java.util.ArrayList;


public class TempResponse {
	private ArrayList<Integer> routeArray;
	private long totalDistance;
	public ArrayList<Integer> getRouteArray() {
		return routeArray;
	}
	public void setRouteArray(ArrayList<Integer> routeArray) {
		this.routeArray = routeArray;
	}
	public long getTotalDistance() {
		return totalDistance;
	}
	public void setTotalDistance(long totalDistance) {
		this.totalDistance = totalDistance;
	}
	
    
}
