package com.taxgo.taxgorouteplanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;
import io.sentry.Sentry;

@SpringBootApplication
@RestController
public class TaxgorouteplannerApplication {
	private static final Logger logger = LoggerFactory.getLogger(TaxgorouteplannerApplication.class);
	public static void main(String[] args) { 
		try {
			Sentry.init(options -> {
				options.setDsn("https://ecbeab26c9dd4d84b2d06ae7e993ef58@o817884.ingest.sentry.io/5810742");
			});

			logger.info("starting the application ");
			SpringApplication.run(TaxgorouteplannerApplication.class, args);
			logger.info("started the application ");
		} catch (Exception e) {
			logger.error("Error occured on application level");
			Sentry.captureException(e);
		}

	}

}
